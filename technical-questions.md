# Interview Questions

## Sources 
- [Software Testing Help](https://www.softwaretestinghelp.com/c-sharp-interview-questions/)

## 1. What are the fundamental OOP concepts?

- `Encapsulation` – The Internal representation of an object is hidden from the view outside object’s definition. Only the required information can be accessed whereas the rest of the data implementation is hidden.
- `Abstraction` – It is a process of identifying the critical behavior and data of an object and eliminating the irrelevant details.
- `Inheritance` – It is the ability to create new classes from another class. It is done by accessing, modifying and extending the behavior of objects in the parent class.
- `Polymorphism` – The name means, one name, many forms. It is achieved by having multiple methods with the same name but different implementations.

## 2. What is Managed and Unmanaged code?
`Managed code` is a code which is executed by CLR (Common Language Runtime) i.e all application code based on .Net Platform. It is considered as managed because of the .Net framework which internally uses the garbage collector to clear up the unused memory.

`Unmanaged code` is any code that is executed by application runtime of any other framework apart from .Net. The application runtime will take care of memory, security and other performance operations.

## 3. What is an Interface?

An Interface is a class with no implementation. The only thing that it contains is the declaration of methods, properties, and events.

## 4. What are the different types of classes in C#?

- `Partial class` – Allows its members to be divided or shared with multiple .cs files. It is denoted by the keyword Partial.
- `Sealed class` – It is a class which cannot be inherited. To access the members of a sealed class, we need to create the object of the class.  It is denoted by the keyword Sealed.
- `Abstract class` – It is a class whose object cannot be instantiated. The class can only be inherited. It should contain at least one method.  It is denoted by the keyword abstract.
- `Static class` – It is a class which does not allow inheritance. The members of the class are also static.  It is denoted by the keyword static. This keyword tells the compiler to check for any accidental instances of the static class.

## 5. Describe the basic construction of a C# program. Write a simple program that outputs “Hello World” to the console.

A typical C# program consists of a namespace declaration, a class, methods, attributes, a main method, statements, expressions, and comments. A potential example for printing “Hello World” to the console is detailed below.

```
using System;
namespace HelloWorldApplication
{
   class HelloWorld
   {
      static void Main(string[] args)
      {
         Console.WriteLine("Hello World");
         Console.ReadKey();
      }
   }
}
```

## 6. Can you name three ways to pass parameters to a method in C#?

- `Value Parameters`: Passing a parameter to a method by value creates a new storage location for the value parameter. Any changes to the value parameter by the method have no effect on the argument.

- `Reference Parameters`: Passing a parameter to a method by reference can be achieved by using the ref keyword. Instead of creating a new storage location for the parameter, the method accesses the memory location of the argument and passes it as a parameter. Changes made to the parameter will also affect the argument.

- `Output Parameters`: The out keyword allows a method to return two values from a function. It’s similar to passing a reference parameter, except in this case data is being transferred out of the method.

## 7. What is Operator Overloading and how does it work?

Most of the built-in operators available in C# can be overloaded or redefined using the operator keyword. The sample code below depicts the syntax used to implement the addition operator (+) for a user-defined class.

```
public static Rectangle operator+ (Rectangle b, Rectangle c)
{
   Rectangle rectangle = new Rectangle();
   rectangle.length = b.length + c.length;
   rectangle.breadth = b.breadth + c.breadth;
   rectangle.height = b.height + c.height;
   return rectangle;
}
```

## 8. What is the difference between dynamic type variables and object type variables in C#?

The difference between dynamic and object type variables has to do with `when the type checking takes place` during the application lifecycle. `Dynamic type variables` handle type checking at run time, while `Object type variables` handle type checking during compile time.

## 9. How does C# handle encapsulation?

Encapsulation is a classic object-oriented design principle that reduces coupling between objects and encourages maintainable code. It `involves enclosing objects within a logical package by limiting access to implementation details`. In C#, this is accomplished through the access specifiers — `public, private, protected, internal, and protected internal`.

## 10. What is Serialization?

`Serialization` is a process of converting a code to its binary format. Once it is converted to bytes, it can be easily stored and written to a disk or any such storage devices. Serializations are mainly useful when we do not want to lose the original form of the code and it can be retrieved anytime in the future.

Any class which is marked with the attribute `[Serializable]` will be converted to its binary form.

The reverse process of getting the c# code back from the binary form is called Deserialization.

To Serialize an object we need the object to be serialized, a stream which can contain the serialized object and namespace `System.Runtime.Serialization` can contain classes for serialization.

## 11. What are the types of Serialization?

The different types of Serialization are: XML serialization, SOAP, and Binary.

- `XML serialization` – It serializes all the public properties to the XML document. Since the data is in XML format, it can be easily read and manipulated in various formats. The classes reside in System.sml.Serialization.
- `SOAP` – Classes reside in System.Runtime.Serialization. Similar to XML but produces a complete SOAP compliant envelope which can be used by any system that understands SOAP.
- `Binary Serialization` – Allows any code to be converted to its binary form. Can serialize and restore public and non-public properties. It is faster and occupies less space.

## 12. What is an Object and a Class?

- A Class is an encapsulation of properties and methods that are used to represent a real-time entity. It is a data structure that brings all the instances together in a single unit.

- An Object in an instance of a Class. Technically, it is just a block of memory allocated that can be stored in the form of Variables, Array or a Collection.

## 13. Explain Code compilation in C#.

- Compiling the source code into Managed code by C# compiler.
- Combining the newly created code into assemblies.
- Loading the Common Language Runtime(CLR).
- Executing the assembly by CLR.

## 14. Explain Namespaces in C#.

They are used to organize large code projects. “System” is the most widely used namespace in C#. We can create our own namespace and use one namespace in another, which are called Nested Namespaces.

They are denoted by the keyword “namespace”.

## 15. Explain Abstraction.

Abstraction is one of the OOP concepts. It is used to display only the essential features of the class and hides the unnecessary information.

```
A driver of the car should know the details about the Car such as color, name, mirror, steering, gear, brake, etc. What he doesn’t have to know is an Internal engine, Exhaust system.

So, Abstraction helps in knowing what is necessary and hiding the internal details from the outside world. Hiding of the internal information can be achieved by declaring such parameters as Private using the private keyword.
```

## 16. Explain Polymorphism.

Programmatically, Polymorphism means same method but different implementations.

`Compile time polymorphism` is achieved by operator overloading.

`Runtime polymorphism` is achieved by overriding. Inheritance and Virtual functions are used during Runtime Polymorphism.

## 17. How is Exception Handling implemented in C#

- `try` – Contains a block of code for which an exception will be checked.
- `catch` – It is a program that catches an exception with the help of exception handler.
- `finally` – It is a block of code written to execute regardless whether an exception is caught or not.
- `Throw` – Throws an exception when a problem occurs.

## 18. What are C# I/O Classes? What are the commonly used I/O Classes?

C# has System.IO namespace, consisting of classes that are used to perform various operations on files like creating, deleting, opening, closing etc.

- `File` – Helps in manipulating a file.
- `StreamWriter` – Used for writing characters to a stream.
- `StreamReader` – Used for reading characters to a stream.
- `StringWriter` – Used for reading a string buffer.
- `StringReader` – Used for writing a string buffer.
- `Path` – Used for performing operations related to path information.

## 19. What is a Destructor in C#?

A Destructor is used to clean up the memory and free the resources. But in C# this is done by the garbage collector on its own. System.GC.Collect() is called internally for cleaning up. But sometimes it may be necessary to implement destructors manually.


## 20. What is an Abstract Class?

An `Abstract class` is a class which is denoted by abstract keyword and can be used only as a Base class. An Abstract class should always be inherited. An instance of the class itself cannot be created. If we do not want any program to create an object of a class, then such classes can be made abstract.

## 21. What are Boxing and Unboxing?

Converting a value type to reference type is called `Boxing`.

```
int Value1 -= 10;

//————Boxing——————//
object boxedValue = Value1;

//————UnBoxing——————//
int UnBoxing = int (boxedValue);

```

## 22. What is the difference between Continue and Break Statement?

`Break` statement breaks the loop. It makes the control of the program to exit the loop. 

`Continue` statement makes the control of the program to exit only the current iteration. It does not break the loop.

## 23. What is the difference between finally and finalize block?

`finally` block is called after the execution of try and catch block. It is used for exception handling. Regardless of whether an exception is caught or not, this block of code will be executed. Usually, this block will have clean-up code.

`finalize` method is called just before garbage collection. It is used to perform clean up operations of Unmanaged code. It is automatically called when a given instance is not subsequently called.

## 24. What is a Jagged Array?

A Jagged array is `an array whose elements are arrays`. It is also called as the array of arrays. It can be either single or multiple dimensions.

## 25. What is a Delegate? Explain.

A `Delegate` is a variable that holds the reference to a method. Hence it is a function pointer of reference type. All Delegates are derived from `System.Delegate` namespace. Both Delegate and the method that it refers to can have the same signature.

Declaring a delegate: public delegate void AddNumbers(int n);

After the declaration of a delegate, the object must be created of the delegate using the new keyword.

`AddNumbers an1 = new AddNumbers(number);`

The delegate provides a kind of encapsulation to the reference method, which will internally get called when a delegate is called.

```
public delegate int myDel(int number);
    
public class Program
{
    public int AddNumbers(int a)
    {
        int Sum = a + 10;
        return Sum;
    }
    public void Start()
    {
        myDel DelgateExample = AddNumbers;
    }
}
```

In the above example, we have a delegate myDel which takes an integer value as a parameter. Class Program has a method of the same signature as the delegate, called AddNumbers().

If there is another method called Start() which creates an object of the delegate, then the object can be assigned to AddNumbers as it has the same signature as that of the delegate.

## 26. What are the different types of Delegates?

- `Single Delegate` - A delegate which can call a single method.
- `Multicast Delegate` - A delegate which can call multiple methods. + and – operators are used to subscribe and unsubscribe respectively.
- `Generic Delegate` - It does not require an instance of delegate to be defined. It is of three types, Action, Funcs and Predicate.
        
`Action` - In the above example of delegates and events, we can replace the definition of delegate and event using Action keyword. 
The Action delegate defines `a method that can be called on arguments but does not return a result`
```
Public delegate void deathInfo();

Public event deathInfo deathDate;

// Replacing with Action

Public event Action deathDate;
```

`Func` – A Func delegate defines `a method that can be called on arguments and returns a result`.
```
Func <int, string, bool> myDel is same as delegate bool myDel(int a, string b);
```

`Predicate` – Defines `a method that can be called on arguments and always returns the bool`.
```
Predicate<string> myDel is same as delegate bool myDel(string s);
```

## 27. Explain Publishers and Subscribers in Events.

A `Publisher` is a class responsible for publishing a message of different types of other classes. The message is nothing but Event.

`Subscribers` capture the message of the type that it is interested in. 

## 28. What are Synchronous and Asynchronous operations?

`Synchronization` is a way to create a thread-safe code where only one thread can access the resource at any given time.

`Asynchronous` call waits for the method to complete before continuing with the program flow. Synchronous programming badly affects the UI operations, when the user tries to perform time-consuming operations since only one thread will be used.

In `Asynchronous` operation, the method call will immediately return so that the program can perform other operations while the called method completes its work in certain situations.

In C#, Async and Await keywords are used to achieve asynchronous programming. 

## 28. What is Reflection in C#?

`Reflection` is the ability of a code to access the metadata of the assembly during runtime. A program reflects upon itself and uses the metadata to inform the user or modify its behavior. Metadata refers to information about objects, methods.

The namespace `System.Reflection` contains methods and classes that manage the information of all the loaded types and methods. It is mainly used for windows applications, for Example, to view the properties of a button in a windows form.

The `MemberInfo` object of the class reflection is used to discover the attributes associated with a class.

Reflection is implemented in two steps, first, we get the type of the object, and then we use the type to identify members such as methods and properties.

To get type of a class, we can simply use

```
Type mytype = myClass.GetType();
```

Once we have a type of class, the other information of the class can be easily accessed.

```
System.Reflection.MemberInfo Info = mytype.GetMethod(“AddNumbers”);
```

Above statement tries to find a method with name AddNumbers in the class myClass.

## 29. What is a Generic Class?

`Generics` or `Generic class` is used to create classes or objects which do not have any specific data type. The data type can be assigned during runtime, i.e when it is used in the program.

## 30. Explain Get and Set Accessor properties?

`Get` and `Set` are called `Accessors`. These are made use by `Properties`. A property provides a mechanism to read, write the value of a private field. For accessing that private field, these accessors are used.

`Get Property` is used to return the value of a property. `Set Property` accessor is used to set the value.

## 31. What is a Thread? What is Multithreading?

A `Thread` is a set of instructions that can be executed, which will enable our program to perform concurrent processing. `Concurrent` processing helps us do more than one operation at a time. By default, C# has only one thread. But the other threads can be created to execute the code in parallel with the original thread.

Thread has a life cycle. It starts whenever a thread class is created and is terminated after the execution. `System.Threading` is the namespace which needs to be included to create threads and use its members.

```
// CallThread is the target method //
ThreadStart methodThread = new ThreadStart(CallThread);
Thread childThread = new Thread(methodThread);
childThread.Start();
```

C# can execute more than one task at a time. This is done by handling different processes by different threads. This is called MultiThreading.

There are several thread methods that are used to handle the multi-threaded operations:

`Start, Sleep, Abort, Suspend, Resume and Join.`

Most of these methods are self-explanatory.


## 32. Name some properties of Thread Class.

Few Properties of thread class are:

- `IsAlive` – contains value True when a thread is Active.
- `Name` – Can return the name of the thread. Also, can set a name for the thread.
- `Priority` – returns the prioritized value of the task set by the operating system.
- `IsBackground` – gets or sets a value which indicates whether a thread should be a background process or foreground.
- `ThreadState` – describes the thread state.

## 33. What are the different states of a Thread?

- `Unstarted` – Thread is created.
- `Running` – Thread starts execution.
- `WaitSleepJoin` – Thread calls sleep, calls wait on another object and calls join on another thread.
- `Suspended` – Thread has been suspended.
- `Aborted` – Thread is dead but not changed to state stopped.
- `Stopped` – Thread has stopped.

## 34. What are Async and Await?

`Async` and `Await` keywords are used to create `asynchronous methods` in C#.

`Asynchronous programming` means that the process runs independently of main or other processes.

## 35.  What is a Deadlock?

A `Deadlock` is a situation where a process is not able to complete its execution because two or more processes are waiting for each other to finish. This usually occurs in `multi-threading`.

Here a `Shared resource` is being held by a process and another process is waiting for the first process to release it and the thread holding the locked item is waiting for another process to complete.

## 36. Explain Lock, Monitors, and Mutex Object in Threading.

`Lock` keyword ensures that only one thread can enter a particular section of the code at any given time. In the above Example, lock(ObjA) means the lock is placed on ObjA until this process releases it, no other thread can access ObjA.

A `Mutex` is also like a lock but it can work across multiple processes at a time. WaitOne() is used to lock and ReleaseMutex() is used to release the lock. But Mutex is slower than lock as it takes time to acquire and release it.

`Monitor.Enter` and `Monitor.Exit` implements lock internally. a lock is a shortcut for Monitors. lock(objA) internally calls.

```
Monitor.Enter(ObjA);

try {

}
Finally {
    Monitor.Exit(ObjA));
}
```

## 37. What is a Race Condition?

A `Race condition` occurs when two threads access the same resource and are trying to change it at the same time. The thread which will be able to access the resource first cannot be predicted.

If we have two threads, T1 and T2, and they are trying to access a shared resource called X. And if both the threads try to write a value to X, the last value written to X will be saved.

## 38. What is Thread Pooling?

A `Thread pool` is a collection of threads. These threads can be used to perform tasks without disturbing the primary thread. Once the thread completes the task, the thread returns to the pool.

`System.Threading.ThreadPool` namespace has classes which manage the threads in the pool and its operations.

E.g.
`System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SomeTask));`

The above line queues a task. SomeTask methods should have a parameter of type Object.