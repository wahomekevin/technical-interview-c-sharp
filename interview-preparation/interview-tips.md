# Technical Interview Tips

Last set of Tips from Daniel (Microsoft Recruiter)

- Coding fluency is prerequisite for the role, so avoid pseudo-code if you can. Your solutions will be shown to senior engineers at a later date, so it needs to be well-formatted & fully compilable.
- Make sure you ask questions before jumping into a solution. Some questions might be intentionally vague as the interviewer wants to see if you are can figure that you are missing some important parameters. Clarify the question and state if you have assumptions to see if the interviewer thinks the same.
- Remember to take edge and corner cases into consideration.
- Understand the importance of unit testing in Microsoft's engineering environment.
- Approach a large problem by taking it apart into small pieces - the interviewer is as interested in how you approach problems as whether you reach the intended result.
- Don't give up on problems - maybe ask for advice.
- Be passionate, the interviewer want to know that you love coding and programming, and want to hear that you are passionate about it. Enthusiasm for your work, coding and computer science is important to get across.
- It is good to have a headset, as you will be coding over the phone or trough Skype. So practically, it will make the interview easier if you're not holding a phone and coding at the same time!
- Ask questions, to show you are interested and are analytical, showing that you are thinking about the role, the question, the problem etc..
- Talk through your answer and your coding. We are looking for good programmers, but also people who can explain their thought processes and can describe analytically what they are doing and their answers.
- Don't worry if your interview is tough - engineers want to stretch you to see if you can deal with new information as well as what you know.
- Consider that you need to finish the task you are given during the interview. Make sure you get to a working solution at least, but ideally have some time left for testing and optimizing.
- Listen to everything the interviewer says as he/she may be trying to give you hints or steer you in another direction.
- The interviewer might ask you a warm up question such as: 'What projects at Microsoft have you heard about lately?' / 'What is the most interesting code you have written?' / What projects are you working on now?' - Think about possible answers to these and explain them clearly. You may be asked questions based on your CV, so know your CV well!
- Prepare some questions for the engineer - how does he/she structure their day? / what products have they enjoyed working on? / etc.
- Write down all your favorite algorithms the day before, so they are fresh in your mind.
- Get a good night's sleep the night before!