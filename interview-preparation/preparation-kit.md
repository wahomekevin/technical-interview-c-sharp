# Microsoft Interview Preparation Kit

[Online Editor for Coding Practice](https://dotnetfiddle.net/)

## Interview setup

This will be a technical interview that will last `~45 minutes`. Microsoft takes a mix of `academic and industry approach` to the interviewing process. 

This means that we are interested in your thought process & your approach to problem solving, as well as your coding abilities. You may be asked questions that relate to `technical knowledge`, `algorithms`, `coding`, `performance`, `how to test solutions`, & perhaps your `interest in Microsoft products`. 

During the interview, please feel free to ask the interviewer if you are not clear with any of the questions. Also, feel free to talk through a problem when needed.

## Equipment needed

You'll need a `computer with internet access` for the interviews to use `Skype` and an `online coding editor` that comes with it. 

Both you and the interviewer will have access this editor during the interview. We also `recommend using a headset or hands-free device during the interview`, to allow for easier conversation while coding.

## Microsoft Technical Interview Tips

Please be prepared for the engineers to ask you questions in the following areas:

### Microsoft products (i.e. what you use, your favorite one, etc.)
```
VS Code, Windows(Gaming), XBOX ONE X
```
### Anything on your resume

1. [Behavioural Questions]( https://docs.google.com/document/d/16P_soqPss87JamBwesQNBn31ti-GhjTX7q-kWQM3N58/edit)
2. [Resume](https://docs.google.com/document/d/1wXptGXaW1qz0_onB_2K9YDj0JvuCvkJgADZN3fZxS8k/edit)

### Coding ability

1. Practice with Visual Studio.
2. [Hacker Rank](https://www.hackerrank.com/interview/interview-preparation-kit).

### Algorithm Design/Analysis
### System Design
 Specific skills depending on the team – for example if it’s a low-level role you may be asked some networking questions while for more web focussed roles you may be asked about web related experience.

## Areas to prepare, directly from Microsoft Engineers

### Tools

1. [Educative IO Tuts](https://www.educative.io/collection/page/5642554087309312/5679846214598656/240002)
2. [Microverse Curriculum](https://microverse.pathwright.com/library/fast-track-algorithms-data-structures/69123/path/)
3. [Learneroo Challenges](https://www.learneroo.com/modules/106)

### 1. Algorithm Complexity

1. [Top-Coder](https://www.topcoder.com/community/competitive-programming/tutorials/
)
2. [Learneroo](https://www.learneroo.com/modules/106/nodes/559)

You need to know `Big-O`. If you struggle with basic big-O complexity analysis, then you are almost guaranteed not to get hired.

#### What is "Time Complexity"

`Time Complexity` is a measure of how much longer it will take an algorithm to run (in number of operations) as the size of the input increases.

Examples: `best case`, `worst case` or `expected case`.

#### What is "Big-O"?
`Big O notation` is a mathematical notation that describes the limiting behavior of a function when the argument tends towards a particular value or infinity.

`Big-O` is the shorthand used to classify the `time complexity` of algorithms.

##### 1. O(1) - Constant Time

The algorithm performs a `constant number of operations` regardless of the size of the input.

```
Examples
- Access a single number in an array by index.
- Add 2 numbers together
```

##### 2. O(log n) - Logarithmic Time

Rate of growth is relatively slow, so algorithms are really fast.

Examples
```
Binary search on a sorted list. The size that needs to be searched is split in half each time, so the remaining list goes from size n to n/2 to n/4... until either the element is found or there's just one element left. If there are a billion elements in the list, it will take a maximum of 30 checks to find the element or determine that it is not on the list.
```

##### 3. O(n) - Linear Time

The running time of an algorithm grows in proportion to the size of input.

Examples
```
- Search through an unsorted array for a number
- Sum all the numbers in an array
- Access a single number in a LinkedList by index
```

##### 4. O(n log n) Linearithmic Time

`log(n)` is much closer to `n` than to `n squared`, so this running time is closer to linear than to higher running times (and no one actually says "Linearithmic").

Examples
```
Sort an an array with QuickSort or Merge Sort. When recursion is involved, calculating the running time can be complicated. You can often work out a sample case to estimate what the running time will be.
```

##### 5. O(n squared)

The algorithm's running time grows in proportion to the square of the input size, and is common when using nested-loops.

Examples
```
- Printing the multiplication table for a list of numbers
- Insertion Sort
```

##### 6. O(nm)

Some algorithms use nested loops where the outer loop goes through an input n while the inner loop goes through a different input m. 

##### 7. O(2 power n)

Trying out every possible binary string of length n.

Example
```
Crack a password
```

##### 8. O(n!)

This issue shows up when you try to solve a problem by trying out every possibility.

Example
```
- Go through all Permutations of a string
- Traveling Salesman Problem (brute-force solution)
```


### 2. Coding

You will be expected to write some code in at least some of your interviews. You should know `at least one programming language really well` (preferably be C/C++, C#, Java or Python).

You will be expected to `know a fair amount of detail about your favorite programming language`.

Refer: (Unavailable in my country) https://www.wiley.com/en-us/Programming+Interviews+Exposed%3A+Secrets+to+Landing+Your+Next+Job%2C+2nd+Edition-p-9780470121672

How Not To Succeed in a 45 minute interview: https://hackernoon.com/how-not-to-succeed-in-your-45-minute-coding-interview-2eebd46bd6ec

### 3. System Design

#### Articles

- Cracking The Code Interview, Chapter 9: https://drive.google.com/file/d/154ud1kYq3Y5WJbA6fT_srwdnVqoFdcmF/view

- Design PasteBin: https://github.com/donnemartin/system-design-primer/blob/master/solutions/system_design/pastebin/README.md

- System Design questions: https://hackernoon.com/top-10-system-design-interview-questions-for-software-engineers-8561290f0444

- How Not To Design Netflix: https://hackernoon.com/how-not-to-design-netflix-in-your-45-minute-system-design-interview-64953391a054
```
Advice on how to do it.

1. Draw a bog box that represents the system.

2. Zoom-in and break the big box into 5 - 6 components.

3. Briefly discuss the role of each component e.g. `compute`, `storage`, `front-end`, `back-end`, `caching`, `queueing`, `networking`, `load-balancing` etc.
```


- Anatomy of a System Design Interview: https://hackernoon.com/anatomy-of-a-system-design-interview-4cb57d75a53f
```

Rule 1: Reading High Scalability a night before your interview does not make you an expert in Distributed Systems.

Rule 2: Never pretend to be an expert. The person interviewing you almost always understands the domain better than you and can even be an industry expert.

Rule 3: Don’t rush to a solution. Gather requirements, suggest multiple solutions and evaluate them. It is meant to be an open-ended discussion.
```

#### Courses & Videos

- Educative.io: https://www.educative.io/collection/page/5668639101419520/5649050225344512/5668600916475904. 
Go through a couple of these in detail.

- Cracking the Code Interview: https://mail.google.com/mail/u/1/#inbox/FMfcgxvzLhkllmdgscVfHDfdhzwkrqjw?projector=1

### 4. Sorting

Know how to sort. Don't do bubble-sort. You should `know the details of at least one n*log(n) sorting algorithm, preferably two (e.g., quicksort & merge sort).`

Merge sort can be highly useful in situations where quicksort is impractical, so take a look at it.


### 5. Hashtables

Arguably the single most important data structure known to mankind. You absolutely should know how they work. `Be able to implement one using only arrays in your favorite language`, in about the space of one interview.

### 6. Trees

Know about trees; `basic tree construction`, `traversal` & `manipulation` algorithms. 
Familiarize yourself with `binary trees`, `n-ary trees`, & `trie-trees`. 

Be familiar with `at least one type of balanced binary tree`, whether it's a `red/black tree`, a `splay tree` or an `AVL tree`, & know how it's implemented. Understand tree traversal algorithms: `BFS & DFS`, & know the difference between `inorder, postorder, & preorder`.

### 7. Graphs

Graphs are really important at Microsoft. There are `3 basic ways to represent a graph in memory (objects & pointers, matrix, & adjacency list)`; familiarize yourself with `each representation & its pros & cons`. 

You should know the basic graph traversal algorithms: `breadth-first search` & `depth-first search`. Know their `computational complexity`, their `tradeoffs`, & `how to implement them in real code`. If you get a chance, try to study up on fancier algorithms, such as `Dijkstra or A*`.

### 8. Other Data Structures

Educative IO Tuts: https://www.educative.io/d/data_structures

You should study up on as many other data structures & algorithms as possible. You should especially know about the `most famous classes of NP-complete problems`, such as `traveling salesman` & `the knapsack problem`, & `be able to recognize them when an interviewer asks you them in disguise`. Find out what NP-complete means.

[C# Collections](https://programmingwithmosh.com/csharp/csharp-collections/)

### 9. Mathematics

Some interviewers ask basic discrete math questions. This is more prevalent at Microsoft than at other companies because we are surrounded by counting problems, probability problems, & other Discrete Math 101 situations. Spend some time before the interview refreshing your memory on (or teaching yourself) the essentials of `combinatorics & probability`. You should be familiar with `n-choose-k` problems & their ilk – the more the better.

### 10. Operating Systems

Know about `processes`, `threads` & `concurrency issues`. Know about `locks` & `mutexes` & `semaphores` & `monitors` & how they work.

Know about `deadlock` & `livelock` & how to avoid them. Know what resources a process needs, & a thread needs, & how `context switching` works, & how it's initiated by the operating system & underlying hardware. Know a little about `scheduling`. The world is rapidly moving towards `multi-core`, so know the fundamentals of `"modern" concurrency constructs`.


### Additional Tips

- Talk through your thought process about the questions you are asked. In all of Microsoft’s interviews, our engineers are evaluating not only your technical abilities
but also how you approach problems and how you try to solve them.

- Ask clarifying questions if you do not understand the problem or need more information. Many of the questions asked in Microsoft interviews are deliberately underspecified because our engineers are looking to see how you engage the problem. In particular, they are looking to see which areas leap to your mind as the most important piece of the technological puzzle you've been presented.

- Think about ways to improve the solution you'll present. In many cases, the first answer that springs to mind isn't the most elegant solution and may need some refining. It's definitely worthwhile to talk about your initial thoughts to a question, but jumping immediately into presenting a brute force solution will be received less well than taking time to compose a more efficient solution. Typically, the interviewer is more interested in a correct solution than a clever one so focus first on getting a correct, working solution before jumping in to optimize.  Do tell the interviewer about trade-offs you’re making, for example, simplicity versus performance and ideas about how you would improve the solution given more time.

- Throughout the interview the interviewer may be commenting on your code or giving you some hints. Consider this feedback carefully as you may be going into the wrong direction and the interviewer is just trying to put you on the right path. Going in the wrong direction isn't bad but ignoring feedback and forcing your solution despite it being wrong is not what we’re looking for.

- When you are done with coding remember to test your code. Think of as many test cases as possible. Be able to enumerate good tests that will prove your code is solid.