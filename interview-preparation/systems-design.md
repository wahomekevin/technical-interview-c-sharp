# The Anatomy of a Systems Design Interview

Source: https://hackernoon.com/anatomy-of-a-system-design-interview-4cb57d75a53f
Source: http://blog.gainlo.co/index.php/2016/03/08/system-design-interview-question-create-tinyurl-system/


## 1. Requirements Gathering

`You need to have a working system before you can scale it.`

As the first step in your interview, you should ask questions to find the exact scope of the problem.

Ask as many questions as you possibly can.


Twitter Example
```
- Who can post a tweet? (answer: any user)
- Who can read the tweet? (answer: any user — as all tweets are public)
- Will a tweet contain photos or videos (answer: for now, just photos)
- Can a user follow another user? (answer: yes).
- Can a user ‘like’ a tweet? (answer: yes).
- What gets included in the user feed (answer: tweets from everyone whom you are following).
- Is feed a list of tweets in chronological order? (answer: for now, yes).
- Can a user search for tweets (answer: yes).
- Are we designing the client/server interaction or backend architecture or both (answer: we want to understand the interaction between client/server but we will focus on how to scale the backend).
- How many total users are there (answer: we expect to reach 200 Million users in the first year).
- How many daily active users are there (100 million users sign-in everyday)
```

## 2. System Interface Definition

`If you have gathered the requirements and can identify the APIs exposed by the system, you are 50% done.`

Define what APIs are expected from the system. This would not only establish the exact contract expected from the system but would also ensure if you haven’t gotten any requirements wrong.

Twitter Example:
```
- postTweet(user_id, tweet_text, image_url, user_location, timestamp, …) 
- generateTimeline(user_id, current_time) 
- recordUserTweetLike(user_id, tweet_id, timestamp, …)
```

## 3. Back of the Envelope Calculations

It’s always a good idea to estimate the scale of the system you’re going to design. 

This would also help later when you’ll be focusing on `scaling, partitioning, load balancing` and `caching`.

```
- What scale is expected from the system (e.g., number of new tweets, number of tweet views, how many timeline generations per sec., etc.)
- How much storage would we need? This will depend on whether users can upload photos and videos in their tweets?
- What network bandwidth usage are we expecting? This would be crucial in deciding how would we manage traffic and balance load between servers.
```

## 4. Defining the data model

`Defining the data model early will clarify how data will flow among different components of the system.`

Later, it will guide you towards better data partitioning and management.

Candidate should be able to identify various entities of the system, how they will interact with each other and different aspect of data management like storage, transfer, encryption, etc.

Twitter Example
```
- User: UserID, Name, Email, DoB, CreationData, LastLogin, etc.
- Tweet: TweetID, Content, TweetLocation, NumberOfLikes, TimeStamp, etc.
- UserFollows: UserdID1, UserID2
- FavoriteTweets: UserID, TweetID, TimeStamp
```

Which database system should we use? Would `NoSQL like Cassandra` best fit our needs, or we should use `MySQL-like` solution. What kind of blob storage should we use to store photos and videos?

## 5. High-level design

`Draw a block diagram with 5–6 boxes representing core components of your system. You should identify enough components that are needed to solve the actual problem from end-to-end.`

Twitter Example:
```
At a high level, we would need multiple application servers to serve all the read/write requests with load balancers in front of them for traffic distributions.

If we’re assuming that we’ll have a lot more read traffic (as compared to write), we can decide to have separate servers for handling reads v.s writes.

On the backend, we need an efficient database that can store all the tweets and can support a huge number of reads.

We would also need a distributed file storage system for storing photos (and videos) and a search index and infrastructure to enable searching of tweets.
```

## 6. Detailed design for selected components

`Dig deeper into 2–3 components; interviewers feedback should always guide you towards which parts of the system she wants you to explain further.`

You should be able to provide different approaches, their pros and cons, and why would you choose one.

Remember there is no single answer, the only thing important is to consider tradeoffs between different options while keeping system constraints in mind.

Twitter Example:
```
- Since we’ll be storing a huge amount of data, how should we partition our data to distribute it to multiple databases? Should we try to store all the data of a user on the same database? What issues can it cause?
- How would we handle high-traffic users e.g. celebrities who have millions of followers?
- Since user’s timeline will contain the most recent (and relevant) tweets, should we try to store our data in a way that is optimized to scan latest tweets?
- How much and at which layer should we introduce cache to speed things up?
- What components need better load balancing?
```

## 7. Identifying and resolving bottlenecks

`Try to discuss as many bottlenecks as possible and different approaches to mitigate them.`

Is there any single point of failure in our system? What are we doing to mitigate it?

Do we’ve enough replicas of the data so that if we lose a few servers, we can still serve our users?

Similarly, do we’ve enough copies of different services running, such that a few failures will not cause total system shutdown?

How are we monitoring the performance of our service? Do we get alerts whenever critical components fail or their performance degrades?

In short, due to the unstructured nature of software design interviews, candidates who are organized with a clear plan to attack the problem have better chances of success.