# Quicksort Array Example

## Arrays Example
![Graphic](https://s3.amazonaws.com/likeitlearnit/images/QuickSortLarge.png)

### C# Implementation
```
using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static void Main()
    {
        int[] testData = { 5, 8, 1, 3, 7, 10, 2 };
        int[] result = Partition(testData);
    }

    private static int[] Partition(int[] ar)
    {
        var leftSubArray = new List<int>();
        var rightSubArray = new List<int>();

        int pivot = ar[0];

        // partition step			
        for (int a = 1; a < ar.Length; a++)
        {
            int numberToConsider = ar[a];

            if (numberToConsider >= pivot)
                rightSubArray.Add(numberToConsider);
            else
                leftSubArray.Add(numberToConsider);
        }

        // left recursion
        if (leftSubArray.Count > 1)
            leftSubArray = Partition(leftSubArray.ToArray()).ToList();

        // right recursion
        if (rightSubArray.Count > 1)
            rightSubArray = Partition(rightSubArray.ToArray()).ToList();

        // merge step
        string leftArray = string.Join(" ", leftSubArray);
        string rightArray = string.Join(" ", rightSubArray);
        string result = $"{ leftArray } { pivot } { rightArray }";

        Console.WriteLine(result.Trim());
        
        return result
            .Split(' ')
			.Where( s => !string.IsNullOrEmpty(s) )
            .Select(s => int.Parse(s))
            .ToArray();
    }
}
```

## In Place Example
![](https://s3.amazonaws.com/likeitlearnit/images/QuickSortInPlace.png)