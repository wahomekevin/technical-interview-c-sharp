# Two Sum Solution

```
public class Solution {
    // test with target = 9
    // nums = array of numbers
    public int[] TwoSum(int[] nums, int target) {
        
        int first, second;
        var numbersList = new List<int>();
        var numbersArrayAsList = nums.ToList();
        
        for(int a = 0; a < numbersArrayAsList.Count; a++)
        {
            first = numbersArrayAsList[a];
            second = target - first;
            
            // temporarily swap out first number
            // bad hack, will fix later
            var temp = first;
            numbersArrayAsList[a] = 9999;
            
            // look for number
            var secondIndex = numbersArrayAsList.FindIndex( n => n == second );
            if( secondIndex > 0 && secondIndex > a )
            {
                numbersList.Add(a);
                numbersList.Add(secondIndex);
                break;
            }
            
            // swap back number
            numbersArrayAsList[a] = temp;
        }
        
        return numbersList.ToArray();
    }
}
```