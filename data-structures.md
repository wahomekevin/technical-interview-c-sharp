# C# Data Structures

## Source
VCS Kicks - http://www.vcskicks.com/csharp_data_structures.php

## 1. Array

A C# array is basically a list of objects.

`[object type][] myArray = new [object type][number of elements]`

Example
```
int[] myIntArray = new int[5];
int[] myIntArray2 = { 0, 1, 2, 3, 4 };
```

## 2.  ArrayList

The C# data structure, ArrayList, is a dynamic array.

```
ArrayList myArrayList = new ArrayList();
myArrayList.Add(56);
myArrayList.Add("String");
myArrayList.Add(new Form());
```


## 3. List<>

List is the generic version of ArrayList, which means that it behaves exactly the same way, but within a specified type.

```
List<int> intList = new List<int>();
intList.Add(45);
intList.Add(34);
```

## 4. LinkedList<>

A `LinkedList` is a series of objects which instead of having their references indexed (like an Array), stay together by linking to each other, in Nodes.

```
// Declaration

LinkedList<int> list = new LinkedList<int>();
list.AddLast(6);

// Value Retrieval

list.First.Value 
or
list.Last.Value
```

## 5. Dictionary<>

The Dictionary C# data structure is extremely useful data structure since it allows the programmer to handle the index keys.

```
Dictionary<string, int> myDictionary = new Dictionary<string, int>();
myDictionary.Add("one", 1);
myDictionary.Add("twenty", 20);
```

## 6. Hashtable

The C# Hashtable data structure is very much like the Dictionary data structure. A Hashtable also takes in a key/value pair, but it does so as generic objects as opposed to typed data.

The reason is speed. `A C# Hashtable stores items faster than a C# Dictionary`, which sacrifices speed for the sake of order..

## 7. HashSet
This particular C# data structure very strongly resembles the List<> data strucuture.

So what is the difference? A HashSet has the very important characteristic that it does not allow duplicate values. For example:

```
HashSet<int> mySet = new HashSet<int>();
mySet.Add(3);
mySet.Add(5);
mySet.Add(3);
mySet.Add(10);

List<int> myListFromSet = mySet.ToList<int>();
int myInt = myListFromSet[2];
```

## 8. Stack

The Stack class is one of the many C# data structures that resembles an List. Like an List, a stack has an add and get method, with a slight difference in behavior.

To add to a stack data structure, you need to use the `Push` call, which is the Add equivalent of an List. Retrieving a value is slightly different. The stack has a `Pop` call, which returns and removes the last object added. If you want to check the top value in a Stack, use the `Peek` call.

```
Stack<string> stack = new Stack<string>();
stack.Push("1");
stack.Push("2");
stack.Push("3");

while (stack.Count > 0)
{
    MessageBox.Show(stack.Pop());
}
```

## 9. Queue

Another one of the many C# data structures is the Queue. A Queue is very similar to the Stack data structure with one major difference.

Rather than follow a LIFO behavior, a Queue data structure goes by FIFO, which stands for First-In-First-Out. 

```
Queue<string> queue = new Queue<string>();
queue.Enqueue("1");
queue.Enqueue("2");
queue.Enqueue("3");

while (queue.Count > 0)
{
   MessageBox.Show(queue.Dequeue());
}
```
