# Algorithm Problems and Solutions

## 1. Two Sum Problem

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Solution (C#)

```
public class Solution {
    // test with target = 9
    // nums = array of numbers
    public int[] TwoSum(int[] nums, int target) {
        
        int first, second;
        var numbersList = new List<int>();
        var numbersArrayAsList = nums.ToList();
        
        for(int a = 0; a < numbersArrayAsList.Count; a++)
        {
            first = numbersArrayAsList[a];
            second = target - first;
            
            // temporarily swap out first number
            // bad hack, will fix later
            var temp = first;
            numbersArrayAsList[a] = 9999;
            
            // look for number
            var secondIndex = numbersArrayAsList.FindIndex( n => n == second );
            if( secondIndex > 0 && secondIndex > a )
            {
                numbersList.Add(a);
                numbersList.Add(secondIndex);
                break;
            }
            
            // swap back number
            numbersArrayAsList[a] = temp;
        }
        
        return numbersList.ToArray();
    }
}
```